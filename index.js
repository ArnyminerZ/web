const express = require('express')
const fs = require('fs')
const https = require('https')

const app = express()

app.use(express.static('/opt/www/dist'))

const httpPort = process.env.AMZ_WWW_HTTP_PORT
const httpsPort = process.env.AMZ_WWW_HTTPS_PORT

let error = "";
if (!httpPort)
    error += "⚠ Environment variable AMZ_WWW_HTTP_PORT not set\n"
if (!httpsPort)
    error += "⚠ Environment variable AMZ_WWW_HTTPS_PORT not set\n"
if (error !== "")
    return console.error(error)

if (parseInt(httpPort) < 1024)
    error += "⚠ AMZ_WWW_HTTP_PORT should be greater than 1024. Current: " + httpPort + '\n'
if (parseInt(httpsPort) < 1024)
    error += "⚠ AMZ_WWW_HTTPS_PORT should be greater than 1024. Current: " + httpsPort + '\n'
if (error !== "")
    return console.error(error)

console.log("🚗 Starting http server...")
app.listen(httpPort, () => {
    console.log("ℹ Listening on http://localhost:" + httpPort)
})

const credentials = fs.existsSync("/opt/ssl") ? {
    key: fs.readFileSync('/opt/ssl/arnyminerz_com.key', 'utf8'),
    cert: fs.readFileSync('/opt/ssl/arnyminerz_com.crt', 'utf8'),
    ca: [fs.readFileSync('/opt/ssl/arnyminerz_com.ca-bundle', 'utf8')]
} : undefined
if (credentials !== undefined){
    console.log("🚗 Starting https server...")
    const httpsServer = https.createServer(credentials, app);
    httpsServer.listen(httpsPort, () => {
        console.log("ℹ Listening on", "https://localhost:" + httpsPort)
    });
}